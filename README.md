# shell-db-backup
Creates a backup from MySQL database and sends it to sftp server.


## Getting started

###### On the server side:

- Create a directory /var/mysql_backups/
- Create an sftp user with ssh key authentification
- Add public keys from every client to /home/<sftp_user>/.ssh/authorized_keys
- Add to crontab delete_old_backups.sh script - e.g.:
```
00 02 * * 1 /opt/scripts/shell-db-backup/delete_old_backups.sh
```


###### On the client side:

- Make sure mysqldump can authentificate without password
- Create a public/private key pair:
```
ssh-keygen -t rsa
```
- Add the sftp server details to config.sh file (see config.sh.example)
- Add the mysql_backup.sh script to crontab
```
00 01 * * 1 /opt/scripts/shell-db-backup/mysql_backup.sh
```
