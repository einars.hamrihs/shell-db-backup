#!/bin/bash
cd "$(dirname "$0")";
source config.sh
#directory that will be created on ftp and the one that will be deleted
TODAY=$(date +"%Y_%m_%d")
TMPDIR=~/backup
mkdir $TMPDIR
mysql_backup_file=$TMPDIR/backup.sql
echo -n "Database dump"
mysqldump --all-databases > $mysql_backup_file &&
echo -n "Dump compression"
yes n | gzip $mysql_backup_file
echo -n "Uploading files via SFTP... "
sftp ${FTPUSER}@${FTPSERVER}<< EOF
cd mysql_backups/
mkdir ${CLIENT_NAME}-${TODAY}
cd ${CLIENT_NAME}-${TODAY}
put ${TMPDIR}/*
cd ..
exit
EOF
echo "Done."
